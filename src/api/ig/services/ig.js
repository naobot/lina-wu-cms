'use strict';

/**
 * ig service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::ig.ig');
