'use strict';

/**
 * ig controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::ig.ig');
