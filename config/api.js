module.exports = {
  responses: {
    privateAttributes: ['createdAt'],
  },
  rest: {
    defaultLimit: 100,
    maxLimit: 250,
    withCount: true,
  },
};
